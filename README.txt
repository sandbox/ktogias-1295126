Provides options to hide disallowed terms, predefine taxonomy term on node creation and redirect after node submission based on referal's or node's taxonomy.
