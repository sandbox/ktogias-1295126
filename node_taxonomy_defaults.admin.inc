<?php 


/*
 * Overview page
 */
function node_taxonomy_defaults_overview() {
  $entity_type = 'node';

  $header = array(
    t('Node type'),
    t('Taxonomy Fields Settings'),
    t('Operations'),
  );

  $entities = node_type_get_types();
  $rows = array();
  foreach ($entities as $entity) {
    $entity_name = $entity->type;
    $field_names = node_taxonomy_defaults_get_fields($entity_type, $entity_name);
    $fields = array();
    foreach ($field_names as $field) {
      $fields[] = array_merge(field_info_instance($entity_type, $field, $entity_name),  node_taxonomy_defaults_get_preferences($entity_type, $entity_name, $field));
    }
    $row = array(
      theme('node_taxonomy_defaults_entity', array('entity' => $entity)),
      theme('node_taxonomy_defaults_fields', array('fields' => $fields)),
    );
    if (!empty($fields))
      $row[] = l(t('Edit'), 'admin/config/content/node-taxonomy-defaults/edit/' .$entity_type.'/'.$entity_name);
    $rows[] = $row;
  }
  
  if (!$rows) {
    $rows[] = array(array(
      'data' => t('No node types available.'),
      'colspan' => 3,
    ));
  }

  $build['node-taxonomy-defaults_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  
  return $build;
}

/**
 * Edit page.
 */
function node_taxonomy_defaults_edit_form($form, &$form_state, $entity_type, $entity_name) {
  $form = array();
  $form['entity_type'] = array(
    '#type' => 'hidden',
    '#value' => $entity_type,
  );
  $form['entity_name'] = array(
    '#type' => 'hidden',
    '#value' => $entity_name,
  );
  $form['hide'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Hide disallowed terms'),
    '#default_value' => array(),
    '#options' => array(),
  );
  $form['hide_none_selection'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Hide none selection'),
    '#default_value' => array(),
    '#options' => array(),
  );
  $form['preselect'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Preselect term based on referer path'),
    '#default_value' => array(),
    '#options' => array(),
  );
  $form['noexpand'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Do not expand taxonomy menu item'),
    '#default_value' => array(),
  );
  $form['redirect'] = array(
    '#type' => 'radios',
    '#title' => t('Redirect to term path after edit'),
    '#default_value' => NULL,
    '#options' => array('none' => t('None')),
  );


  $field_names = node_taxonomy_defaults_get_fields($entity_type, $entity_name);
  foreach ($field_names as $field) {
    $info = array_merge(field_info_instance($entity_type, $field, $entity_name),  node_taxonomy_defaults_get_preferences($entity_type, $entity_name, $field));
    $form['hide']['#options'][$info['field_name']] = $info['label'];
    $form['hide_none_selection']['#options'][$info['field_name']] = $info['label'];
    $form['preselect']['#options'][$info['field_name']] = $info['label'];
    $form['noexpand']['#options'][$info['field_name']] = $info['label'];
    $form['redirect']['#options'][$info['field_name']] = $info['label'];
    if ($info['hide_disallowed_terms'])
      $form['hide']['#default_value'][] = $info['field_name'];
    if ($info['hide_none_selection'])
      $form['hide_none_selection']['#default_value'][] = $info['field_name'];
    if ($info['preselect_per_path'])
      $form['preselect']['#default_value'][] = $info['field_name'];
    if ($info['never_expand_menu_item'])
      $form['noexpand']['#default_value'][] = $info['field_name'];
    if ($info['redirect_to_taxonomy_path'])
      $form['redirect']['#default_value'] = $info['field_name'];
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate the edit page form submission.
 */
function node_taxonomy_defaults_edit_form_validate($form, &$form_state) {
}

/**
 * Process the edit page form submission.
 */

function node_taxonomy_defaults_edit_form_submit($form, &$form_state) {
  $field_names = node_taxonomy_defaults_get_fields($form_state['values']['entity_type'], $form_state['values']['entity_name']);
  foreach ($field_names as $field) {
    $preferences = array(
      'hide_disallowed_terms' => 0,
      'hide_none_selection' => 0,
      'preselect_per_path' => 0,
      'redirect_to_taxonomy_path' => 0,
      'never_expand_menu_item' => 0,
    );
    
    if ($form_state['values']['hide'][$field])
      $preferences['hide_disallowed_terms'] = 1;
    if ($form_state['values']['hide_none_selection'][$field])
      $preferences['hide_none_selection'] = 1;
    if ($form_state['values']['preselect'][$field])
      $preferences['preselect_per_path'] = 1;
    if ($form_state['values']['noexpand'][$field])
      $preferences['never_expand_menu_item'] = 1;
    if ($form_state['values']['redirect'] == $field)
      $preferences['redirect_to_taxonomy_path'] = 1;
      node_taxonomy_defaults_set_preferences($form_state['values']['entity_type'], $form_state['values']['entity_name'], $field, $preferences);
  }
  $form_state['redirect'] = 'admin/config/content/node-taxonomy-defaults';
}

function theme_node_taxonomy_defaults_entity($variables){
  $output = check_plain($variables['entity']->name).' (<small>'.t('Machine name:').' '.check_plain($variables['entity']->type).'</small>)';
  if (empty($output)) {
    $output = '<i>' . t('Empty') . '</i>';
  }
  return $output;
}

function theme_node_taxonomy_defaults_fields($variables){
  $items = array();
  foreach ($variables['fields'] as $field) {
    $preferences = array();
    if ($field['hide_disallowed_terms'])
      $preferences[] = t('Hide disallowed terms');
    if ($field['hide_none_selection'])
      $preferences[] = t('Hide none selection');
    if ($field['preselect_per_path'])
      $preferences[] = t('Preselect term based on referer path');
    if ($field['redirect_to_taxonomy_path'])
      $preferences[] = t('Redirect to term path after edit');
    if ($field['never_expand_menu_item'])
      $preferences[] = t('Do not expand taxonomy menu item');
    $items[] = check_plain($field['label']).' (<small>'.t('Field name:').' '.check_plain($field['field_name']).'</small>)'.theme('item_list', array('items' => $preferences));
  }
  return theme('item_list', array('items' => $items));
}
